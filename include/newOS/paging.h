#ifndef PAGING_H_
#define PAGING_H_

#include <stdint.h>

typedef struct{
	uint32_t present : 1;
	uint32_t rw : 1;
	uint32_t user : 1;
	uint32_t accessed : 1;
	uint32_t dirty : 1;
	uint32_t unused : 7;
	uint32_t frame : 20;
} page_t;

typedef struct{
	page_t pages[1024];
} page_directory_t;

typedef struct{
	page_t pages[1024];
} page_table_t;

void paging_init();
page_t create_page();

#endif // PAGING_H_