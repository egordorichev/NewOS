#ifndef IDT_H_
#define IDT_H_

#include <stdint.h>

struct idt_entry{	
	uint16_t base_lo;
	uint16_t sel;	
	uint8_t always0;
	uint8_t flags;
	uint16_t base_hi;
} __attribute__((packed));

struct idt_ptr{
	uint16_t limit;
	unsigned int base;
} __attribute__((packed));

void idt_init();
void idt_setgate(uint8_t num, unsigned long base, uint16_t sel, uint8_t flags);

#endif // IDT_H_