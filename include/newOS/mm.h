#ifndef MM_H_
#define MM_H_

#include <stdint.h>

void mm_init();
void *mm_maloc(int size);
void mm_free(void *pointer);

#endif // MM_H_