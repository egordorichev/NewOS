#ifndef GDT_H_
#define GDT_H_

#include <stdint.h>

struct gdt_entry{
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_middle;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
} __attribute__((packed));

struct gdt_ptr{
	uint16_t limit;
	unsigned int base;
} __attribute__((packed));

void gdt_init();
void gdt_setgate(int num, unsigned long base, unsigned long limit, uint8_t access, uint8_t gran);
extern void gdt_flush();

#endif // GDT_H_