#ifndef CLOCK_H_
#define CLOCK_H_

typedef struct{
	int year;
	int month;
	int day;
} date_t;

typedef struct{
	int hour;
	int minute;
	int second;
} time_t;

date_t getdate();
time_t gettime();

char *date_tostring(date_t date);
char *time_tostring(time_t date);

#endif // CLOCK_H_