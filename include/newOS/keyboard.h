#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include <stdint.h>
#include <stdbool.h>

#define KEYBOARD_SC_CAPSLOCK 0x3a
#define KEYBOARD_SC_LEFT_ALT 0x38
#define KEYBOARD_SC_LEFT_CTRL 0x1d
#define KEYBOARD_SC_LEFT_SHIFT 0x2a
#define KEYBOARD_SC_RIGHT_SHIFT 0x36
#define KEYBOARD_SC_BACKSPACE 0x0e
#define KEYBOARD_SC_TAB 0x0f
#define KEYBOARD_SC_A 0x1e
#define KEYBOARD_SC_B 0x30
#define KEYBOARD_SC_D 0x20
#define KEYBOARD_SC_E 0x12
#define KEYBOARD_SC_F 0x21
#define KEYBOARD_SC_K 0x25
#define KEYBOARD_SC_T 0x14
#define KEYBOARD_SC_U 0x16
#define KEYBOARD_SC_W 0x11
#define KEYBOARD_SC_X 0x2d
#define KEYBOARD_SC_Y 0x15
#define KEYBOARD_SC_ENTER 0x1c
#define KEYBOARD_SC_DELETE 0x53
#define KEYBOARD_SC_INSERT 0x52
#define KEYBOARD_SC_END 0x4f
#define KEYBOARD_SC_HOME 0x47
#define KEYBOARD_SC_PGDN 0x51
#define KEYBOARD_SC_PGUP 0x49
#define KEYBOARD_SC_UP 0x48
#define KEYBOARD_SC_LEFT 0x4b
#define KEYBOARD_SC_RIGHT 0x4d
#define KEYBOARD_SC_DOWN 0x50

typedef struct{
	bool shift;
	bool alt;
	bool control;
	bool capslock;
	bool numlock;
	bool gui;
} keyboard_state_t;

void keyboard_init();
void keyboard_handler();
char keyboard_getchar();
char keyboard_getscancode();
char keyboard_sctochar(uint8_t scancode);
bool keyboard_iskeyup(uint8_t scancode);
bool keyboard_iskeydown(uint8_t scancode);
keyboard_state_t keyboard_getstate();

#endif // KEYBOARD_H_