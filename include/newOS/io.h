#ifndef IO_H_
#define IO_H_

#include <stdint.h>

uint8_t inportb(uint16_t port);
void outportb(uint16_t port, uint8_t data);

#define inb(port) (inportb(port))
#define outb(port, data) (outportb(port, data))

#define asm __asm__
#define volatile __volatile__

#endif // IO_H_