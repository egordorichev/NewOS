#ifndef PIT_H_
#define PIT_H_

void pit_init();
void pit_sleep(int ticks);
int pit_getticks();

#endif // PIT_H_