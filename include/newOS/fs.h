#ifndef FS_H_
#define FS_H_

#include <stdint.h>

typedef struct{
	char *name;
	uint32_t flags;
} FILE;

void fs_init();

#endif // FS_H_