#ifndef IRQ_H_
#define IRQ_H_

#include "isr.h"

extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

typedef void (*irq_handler_t)(struct registers_t *regs);

void irq_installhandler(int irq, irq_handler_t handler);
void irq_uninstallhandler(int irq);
void irq_remap();
void irq_handler(struct registers_t *regs);
void irq_init();

#endif // IRQ_H_