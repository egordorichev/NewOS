#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <stdint.h>

#define VGA_BLACK 0
#define VGA_BLUE 1
#define VGA_GREEN 2
#define VGA_CYAN 3
#define VGA_RED 4
#define VGA_MAGENTA 5
#define VGA_BROWN 6
#define VGA_LIGHTGREY 7
#define VGA_DARK_GREY 8
#define VGA_LIGHTBLUE 9
#define VGA_LIGHTGREEN 10
#define VGA_LIGHTCYAN 11
#define VGA_LIGHTRED 12
#define VGA_LIGHTMAGENTA 13
#define VGA_LIGHTBROWN 14
#define VGA_WHITE 15

typedef struct{
	uint16_t *video_memory;
	uint8_t color;
	int cursor_x;
	int cursor_y;
} terminal_t;

void terminal_init(terminal_t *terminal, uint16_t *video_address, int foreground, int background);
void terminal_updatecursor(terminal_t *terminal);
void terminal_movecursor(terminal_t *terminal, int x, int y);
void terminal_putchar(terminal_t *terminal, char c);
void terminal_write(terminal_t *terminal, char *string);
void terminal_setcolor(terminal_t *terminal, int foreground, int background);
void terminal_clear(terminal_t *terminal);
void terminal_scroll(terminal_t *terminal);
uint8_t terminal_createcolor(int foreground, int background);
uint16_t terminal_createentry(char c, uint8_t color);
terminal_t *get_currentterminal();
void set_currentterminal(terminal_t *terminal);

#endif // TERMINAL_H_