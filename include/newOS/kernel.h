#ifndef KERNEL_H_
#define KERNEL_H_

#include <newOS/isr.h>

int kernel_main();
void kernel_panic(char const *file, int line, char const *failure, struct registers_t *registers);

#endif // KERNEL_H_