#include <newOS/terminal.h>
#include <newOS/io.h>

#include <string.h>

terminal_t *_current_terminal;

void terminal_init(terminal_t *terminal, uint16_t *video_address, int foreground, int background){
	terminal->video_memory = video_address;
	terminal->cursor_x = 0;
	terminal->cursor_y = 0;

	terminal_setcolor(terminal, foreground, background);	
	terminal_clear(terminal);
}

void terminal_updatecursor(terminal_t *terminal){
	uint16_t cursor_location = terminal->cursor_y * 80 + terminal->cursor_x;
	
	outb(0x3D4, 14);                  
	outb(0x3D5, cursor_location >> 8); 
	outb(0x3D4, 15);                  
	outb(0x3D5, cursor_location);	
}

void terminal_movecursor(terminal_t *terminal, int x, int y){
	terminal->cursor_x = x;
	terminal->cursor_y = y;
	
	terminal_updatecursor(terminal);
}

void terminal_putchar(terminal_t *terminal, char c){
	switch(c){
		case '\n': 
			terminal->cursor_x = 0;
			terminal->cursor_y++;

			if(terminal->cursor_y >= 25){
				terminal_scroll(terminal);
			}
		break;
		case '\b': 
			terminal->cursor_x--;
			
			terminal_putchar(terminal, ' ');
			
			terminal->cursor_x--;
		break;
		case '\r':
			terminal->cursor_x = 0;
		break;
		case '\t':
			terminal_write(terminal, "    ");
		break;
		default: 
			terminal->video_memory[terminal->cursor_y * 80 + terminal->cursor_x] = terminal_createentry(c, terminal->color);
			
			if(++terminal->cursor_x >= 80){
				terminal->cursor_x = 0;
				
				if(++terminal->cursor_y >= 25){
					terminal_scroll(terminal);
				}
			}
		break;
	}
	
	terminal_updatecursor(terminal);
}

void terminal_write(terminal_t *terminal, char *string){
	for(int i = 0; i < strlen(string); i++){
		if(string[i] == '\x1B'){
			if(i < strlen(string) - 2){
				i += 2;
				
				terminal_setcolor(terminal, string[i] - '0', VGA_BLACK);
			}
		} else {
			terminal_putchar(terminal, string[i]);
		}
	}
}

void terminal_setcolor(terminal_t *terminal, int foreground, int background){
	terminal->color = terminal_createcolor(foreground, background);
}

void terminal_clear(terminal_t *terminal){
	for(int i = 0; i < 80 * 25; i++){
		terminal->video_memory[i] = terminal_createentry(' ', terminal->color);
	}
	
	terminal->cursor_x = 0;
	terminal->cursor_y = 0;
	
	terminal_updatecursor(terminal);
}

void terminal_scroll(terminal_t *terminal){
	for(int i = 0; i < 25 * 80; i++){
		terminal->video_memory[i] = terminal->video_memory[i + 80];
	}

	terminal->cursor_y--;

	terminal_updatecursor(terminal);
}

uint8_t terminal_createcolor(int foreground, int background){
	return foreground | background << 4;
}

uint16_t terminal_createentry(char c, uint8_t color){
	return (uint16_t)c | (uint16_t)color << 8;
}

terminal_t *get_currentterminal(){
	return _current_terminal;	
}

void set_currentterminal(terminal_t *terminal){
	_current_terminal = terminal;
}