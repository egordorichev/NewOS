#include <newOS/clock.h>
#include <newOS/io.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>

char *months[] = {
	"",
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
};

char *days[] = {
	"",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday"
};

date_t getdate(){
	date_t date;
	unsigned int year, month, day;

	outb(0x70, 0x95);
	outb(0x70, 9);
	
	year = inb(0x71);
	date.year = year - ((unsigned int)year/16) * 6;

	outb(0x70, 8);

	month = inb(0x71);
	date.month = month - ((unsigned int)month/16) * 6;

	outb(0x70, 7);
	
	day = inb(0x71);
	date.day = day - ((unsigned int)day/16) * 6;
	
	return date;
}

time_t gettime(){
	time_t time;
	unsigned int hour, minute, second;

	outb(0x70, 0x95);
	outb(0x70, 4);
	
	hour = inb(0x71);
	time.hour = hour - ((unsigned int)hour/16) * 6;

	outb(0x70, 2);
	
	minute = inb(0x71);
	time.minute = minute - ((unsigned int)minute/16) * 6;	
	
	outb(0x70, 0);
	
	second = inb(0x71);
	time.second = second - ((unsigned int)second/16) * 6;
	
	return time;
}

char *date_tostring(date_t date){
	char *string = malloc(256);
	
	//sprintf(string, "%s %i", days[date.day], date.day);
	
	return string;
}

char *time_tostring(time_t date){
	char *string = malloc(256);
	
	//sprintf(string, "%i %i %i", date.hour, date.minute, date.second);
	
	return string;
}