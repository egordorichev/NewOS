#include <newOS/irq.h>
#include <newOS/idt.h>
#include <newOS/io.h>

irq_handler_t handlers[16];

void irq_installhandler(int irq, irq_handler_t handler){
	handlers[irq] = handler;
}

void irq_uninstallhandler(int irq){
	handlers[irq] = 0;
}

void irq_remap(void){
	outportb(0x20, 0x11);
	outportb(0xA0, 0x11);
	outportb(0x21, 0x20);
	outportb(0xA1, 0x28);
	outportb(0x21, 0x04);
	outportb(0xA1, 0x02);
	outportb(0x21, 0x01);
	outportb(0xA1, 0x01);
	outportb(0x21, 0x0);
	outportb(0xA1, 0x0);
}

void irq_init(){
	irq_remap();

	idt_setgate(32, (unsigned)irq0, 0x08, 0x8E);
	idt_setgate(33, (unsigned)irq1, 0x08, 0x8E);
	idt_setgate(34, (unsigned)irq2, 0x08, 0x8E);
	idt_setgate(35, (unsigned)irq3, 0x08, 0x8E);
	idt_setgate(36, (unsigned)irq4, 0x08, 0x8E);
	idt_setgate(37, (unsigned)irq5, 0x08, 0x8E);
	idt_setgate(38, (unsigned)irq6, 0x08, 0x8E);
	idt_setgate(39, (unsigned)irq7, 0x08, 0x8E);
	idt_setgate(40, (unsigned)irq8, 0x08, 0x8E);
	idt_setgate(41, (unsigned)irq9, 0x08, 0x8E);
	idt_setgate(22, (unsigned)irq10, 0x08, 0x8E);
	idt_setgate(43, (unsigned)irq11, 0x08, 0x8E);
	idt_setgate(44, (unsigned)irq12, 0x08, 0x8E);
	idt_setgate(45, (unsigned)irq13, 0x08, 0x8E);
	idt_setgate(46, (unsigned)irq14, 0x08, 0x8E);
	idt_setgate(47, (unsigned)irq15, 0x08, 0x8E);
}

void irq_handler(struct registers_t *regs){
	irq_handler_t handler = handlers[regs->int_no - 32];
	
	if(handler){
		handler(regs);
	}

	if(regs->int_no >= 40){
		outportb(0xA0, 0x20);
	}

	outportb(0x20, 0x20);
}