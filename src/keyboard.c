#include <newOS/keyboard.h>
#include <newOS/irq.h>
#include <newOS/io.h>

uint8_t keyboard_keys[128] = {
	0, 27, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '\b',
	'\t', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',
	0, 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', 0,
	'\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 0,
	'*',
	0,
	' ',
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 
	0, 
	0,  
	0,  
	0,   
	'-',
	0, 
	0,   
	0,  
	'+', 
	0,   
	0,  
	0,   
	0,   
	0, 
	0, 0, 0,
	0,  
	0, 
	0,  
};

uint8_t keyboard_keysshift[128] = {
	0, 27, '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '\b',
	'\t', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '\n',
	0, 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', '~', 0, /* lshift */
	'|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', 0, /* rshift */
	'*',
	0,
	' ',
	0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0,
	0,
	0,
	0,
	0,
	'-',
	0,
	0,  
	0, 
	'+', 
	0,  
	0,   
	0,  
	0,  
	0,
	0, 0, 0,
	0,
	0,
	0,
};

uint8_t keyboard_buffer[256];
int keyboard_buffersize;
keyboard_state_t _state;

char keyboard_sctochar(uint8_t scancode){
	if(scancode & 0x80){ 
		// Keyup
		if(scancode == 0xAA || scancode == 0xB6){
			_state.shift = 0;
		}
	} else {
		// Keydown
		switch(scancode){
			case KEYBOARD_SC_LEFT_SHIFT: case KEYBOARD_SC_RIGHT_SHIFT: _state.shift = 1; break;
			case KEYBOARD_SC_CAPSLOCK: _state.capslock = !_state.capslock; break;
			case KEYBOARD_SC_BACKSPACE:
				return '\b';
			break;
			case KEYBOARD_SC_ENTER: 
				return '\n';
			break;
			default:
				if(_state.capslock){
					if(_state.shift){
						return keyboard_keys[scancode];
					} else {
						return keyboard_keysshift[scancode];
					}
				} else {
					if(_state.shift){
						return keyboard_keysshift[scancode];
					} else {
						return keyboard_keys[scancode];
					}
				}
			break;
		}	
	}

	return 0;
}

void keyboard_clearbuffer(){
	keyboard_buffersize = 0;
}

void keyboard_writetobuffer(char c){
	if(keyboard_buffersize < 256){
		keyboard_buffer[keyboard_buffersize] = c;
		keyboard_buffersize++;
	}
}

char keyboard_getchar(){
	if(keyboard_buffersize){
		keyboard_buffersize--;
		return keyboard_sctochar(keyboard_buffer[keyboard_buffersize]);
	}

	return 0;
}

char keyboard_getscancode(){
	if(keyboard_buffersize){
		keyboard_buffersize--;
		return keyboard_buffer[keyboard_buffersize];
	}

	return 0;
}

bool keyboard_iskeyup(uint8_t scancode){
	if(scancode & 0x80){
		return true;
	}

	return false;
}

bool keyboard_iskeydown(uint8_t scancode){
	if(scancode & 0x80){
		return false;
	}

	return true;
}

keyboard_state_t keyboard_getstate(){
	return _state;
}

void keyboard_handler(){
	keyboard_writetobuffer(inb(0x60));
}

void keyboard_init(){
	irq_installhandler(1, &keyboard_handler);	

	keyboard_buffersize = 0;
}