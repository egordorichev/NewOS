#include <newOS/mm.h>
#include <newOS/paging.h>
#include <newOS/io.h>

#include <stdio.h>
#include <memory.h>

extern uint32_t end;
uint32_t *placement_address = &end;

void mm_init(){

}

void *mm_malloc(int size){
	void *tmp = (void *)placement_address;
	placement_address += size;
	
	return tmp;
}

void mm_free(void *pointer){
	
}