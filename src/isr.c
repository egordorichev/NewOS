#include <newOS/isr.h>
#include <newOS/idt.h>
#include <newOS/kernel.h>

#include <stdio.h>

unsigned char *exception_messages[] ={
	"Division By Zero",
	"Debug",
	"Non Maskable Interrupt",
	"Breakpoint",
	"Into Detected Overflow",
	"Out of Bounds",
	"Invalid Opcode",
	"No Coprocessor",
	"Double Fault",
	"Coprocessor Segment Overrun",
	"Bad TSS",
	"Segment Not Present",
	"Stack Fault",
	"General Protection Fault",
	"Page Fault",
	"Unknown Interrupt",
	"Coprocessor Fault",
	"Alignment Check (486+)",
	"Machine Check (Pentium/586+)",
	"Reserved"
};

void isr_init(){
	idt_setgate(0, (unsigned)isr0, 0x08, 0x8E);
	idt_setgate(1, (unsigned)isr1, 0x08, 0x8E);
	idt_setgate(2, (unsigned)isr2, 0x08, 0x8E);
	idt_setgate(3, (unsigned)isr3, 0x08, 0x8E);
	idt_setgate(4, (unsigned)isr4, 0x08, 0x8E);
	idt_setgate(5, (unsigned)isr5, 0x08, 0x8E);
	idt_setgate(6, (unsigned)isr6, 0x08, 0x8E);
	idt_setgate(7, (unsigned)isr7, 0x08, 0x8E);
	idt_setgate(8, (unsigned)isr8, 0x08, 0x8E);
	idt_setgate(9, (unsigned)isr9, 0x08, 0x8E);
	idt_setgate(10, (unsigned)isr10, 0x08, 0x8E);
	idt_setgate(11, (unsigned)isr11, 0x08, 0x8E);
	idt_setgate(12, (unsigned)isr12, 0x08, 0x8E);
	idt_setgate(13, (unsigned)isr13, 0x08, 0x8E);
	idt_setgate(14, (unsigned)isr14, 0x08, 0x8E);
	idt_setgate(15, (unsigned)isr15, 0x08, 0x8E);
	idt_setgate(16, (unsigned)isr16, 0x08, 0x8E);
	idt_setgate(17, (unsigned)isr17, 0x08, 0x8E);
	idt_setgate(18, (unsigned)isr18, 0x08, 0x8E);
	idt_setgate(19, (unsigned)isr19, 0x08, 0x8E);
	idt_setgate(20, (unsigned)isr20, 0x08, 0x8E);
	idt_setgate(21, (unsigned)isr21, 0x08, 0x8E);
	idt_setgate(22, (unsigned)isr22, 0x08, 0x8E);
	idt_setgate(23, (unsigned)isr23, 0x08, 0x8E);
	idt_setgate(24, (unsigned)isr24, 0x08, 0x8E);
	idt_setgate(25, (unsigned)isr25, 0x08, 0x8E);
	idt_setgate(26, (unsigned)isr26, 0x08, 0x8E);
	idt_setgate(27, (unsigned)isr27, 0x08, 0x8E);
	idt_setgate(28, (unsigned)isr28, 0x08, 0x8E);
	idt_setgate(29, (unsigned)isr29, 0x08, 0x8E);
	idt_setgate(30, (unsigned)isr30, 0x08, 0x8E);
	idt_setgate(31, (unsigned)isr31, 0x08, 0x8E);
}

void isr_faulthandler(struct registers_t *regs){
	if(regs->int_no < 32){
		kernel_panic(__FILE__, __LINE__, exception_messages[regs->int_no], regs);
	}
}