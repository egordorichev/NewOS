#include <newOS/pit.h>
#include <newOS/idt.h>
#include <newOS/isr.h>
#include <newOS/irq.h>
#include <newOS/io.h>

#include <stdio.h>

int tick;

static void pit_handler(struct registers_t *regs){
	tick++;
}

void pit_init(){
	tick = 0;
	
	irq_installhandler(0, pit_handler);

	uint32_t divisor = 1193180 / 50;

 	outb(0x43, 0x36);

	uint8_t l = (uint8_t)(divisor & 0xFF);
	uint8_t h = (uint8_t)((divisor >> 8) & 0xFF);

	outb(0x40, l);
	outb(0x40, h);
}

void pit_sleep(int ticks){
	int totalticks = ticks + tick; 
	
	while(tick < totalticks);
}

int pit_getticks(){
	return tick;
}