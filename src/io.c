#include <newOS/io.h>

uint8_t inportb(uint16_t port){
	uint8_t result;
	
	asm volatile("inb %1, %0" : "=a" (result) : "dN" (port));
	
	return result;
}

void outportb(uint16_t port, uint8_t data){
	asm volatile("outb %1, %0" : : "dN" (port), "a" (data));
}