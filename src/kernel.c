#include <newOS/newOS.h>
#include <newOS/kernel.h>
#include <newOS/terminal.h>
#include <newOS/gdt.h>
#include <newOS/idt.h>
#include <newOS/isr.h>
#include <newOS/irq.h>
#include <newOS/pit.h>
#include <newOS/paging.h>
#include <newOS/io.h>
#include <newOS/keyboard.h>
#include <newOS/clock.h>
#include <newOS/multiboot.h>
#include <newOS/mm.h>
#include <newOS/fs.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern uint32_t end;
terminal_t terminal;

int kernel_main(){
	terminal_init(&terminal, (uint16_t *)0xB8000, VGA_WHITE, VGA_BLACK);
	set_currentterminal(&terminal);
	
	// ----------
	
	gdt_init();
	idt_init();
	isr_init();
	irq_init();
	
	asm("sti");
	
	mm_init();
	paging_init();

	pit_init();
	keyboard_init();
	
	// ----------
	
	terminal_setcolor(&terminal, VGA_GREEN, VGA_BLACK);
	printf("\n       `7MN.   `7MF\'                           .g8\"\"8q.    .M\"\"\"bgd\n         MMN.    M                           .dP'    `YM. ,MI    \"Y\n         M YMb   M  .gP\"Ya `7M\'    ,A    `MF'dM'      `MM `MMb.\n         M  `MN. M ,M'   Yb  VA   ,VAA   ,V  MM        MM   `YMMNq.\n         M   `MM.M 8M\"\"\"\"\"\"   VA ,V  VA ,V   MM.      ,MP .     `MM\n         M     YMM YM.    ,    VVV    VVV    `Mb.    ,dP' Mb     dM\n       .JML.    YM  `Mbmmd\'     W      W       `\"bmmd\"'   P\"Ybmmd\"\n\n");
	terminal_setcolor(&terminal, VGA_WHITE, VGA_BLACK);
	printf("\t\t\t\t\t\t\t\t  v%s\n\n", NEWOS_VERSION_STRING);
	
	char *promt = "> ";
	
	while(1){
		terminal_setcolor(&terminal, VGA_LIGHTRED, VGA_BLACK);
		printf(promt);
		terminal_setcolor(&terminal, VGA_WHITE, VGA_BLACK);
		
		char *line = readline();
		
		printf("\n");
		
		if(strcmp(line, "help") == 0){
			printf("NewOS v%s build %s\n * help - Display this message\n * version - Display version\n * login - Login into NewOS\n", NEWOS_VERSION_STRING, NEWOS_BUILD_STRING);
		} else if(strcmp(line, "version") == 0){
			printf("NewOS v%s build %s\n", NEWOS_VERSION_STRING, NEWOS_BUILD_STRING);
		} else if(strcmp(line, "login") == 0){
			terminal_clear(&terminal);
			
			while(1){		
				printf("Login: ");
			
				char *login = readline();
				
				printf("\nPassword: ");
				
				char *password = readpass();
				
				printf("\n");
				
				if(strcmp(login, "root") == 0 && strcmp(password, "newOS") == 0){
					promt = "root@newOS> ";
					
					break;
				} else {
					printf("Invalid login or password.\n");
				}
			}
		} else if(strcmp(line, "") == 0){ 
			// Skip
		} else {
			printf("Unknown command\n");
		}
	}
	
	return 0;
}

void kernel_panic(char const *file, int line, char const *failure, struct registers_t *registers){
	printf("Kernel panic at %s, line %i\n\t%s\n", file, line, failure);

	if(registers){
		printf("\nds=%x edi=%x esi=%x ebp=%x\nesp=%x ebx=%x edx=%x ecx=%x\neax=%x int_no=%x err_code=%x eip=%x\ncs=%x eflags=%x useresp=%x ss=%x\n", registers->ds, registers->edi, registers->esi, registers->ebp, registers->esp, registers->ebx, registers->edx, registers->eax, registers->int_no, registers->err_code, registers->eip, registers->cs, registers->eflags, registers->useresp, registers->ss);
	}
	
	for(;;){
		__asm("cli; hlt");
	}
}