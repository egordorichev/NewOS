CFLAGS = -std=c99 -ffreestanding -m32 -I libc/include -I include -L libc
CSRC = src/kernel.o src/io.o src/terminal.o src/gdt.o src/idt.o src/isr.o src/irq.o src/paging.o src/mm.o src/pit.o src/keyboard.o src/clock.o src/fs.o
LIBC = libc/src/stdlib.o libc/src/stdio.o libc/src/string.o libc/src/memory.o
ASSRC = src/boot.o src/int.o
ASFLAGS = -f elf
KERNEL_NAME = NewOS
ISO_NAME = NewOS.iso

all: libc $(CSRC) $(ASSRC) link clean

createiso:
	cp $(KERNEL_NAME) ./iso/boot/kernel
	grub2-mkrescue -o $(ISO_NAME) iso/

libc: $(LIBC)
	ar rcs liblibc.a $(LIBC)

%.o: %.cc
	gcc $(FLAG) -c $< -o  $@
	
%.o: %.S
	nasm $(ASFLAGS) $< -o  $@
	
%.o: %.asm
	nasm $(ASFLAGS) $< -o  $@

link:
	ld -melf_i386 -T linker.ld $(CSRC) -L ./ --library libc $(ASSRC) -o $(KERNEL_NAME)
clean:
	rm src/*.o libc/src/*.o

run:
	qemu -kernel $(KERNEL_NAME)
	
runiso:
	qemu $(ISO_NAME)