#ifndef STDBOOL_H_
#define	STDBOOL_H_	

#define	__bool_true_false_are_defined

#ifndef __cplusplus

#define	false 0
#define	true 1

#define	bool _Bool

#endif // __cplusplus

#endif // STDBOOL_H_