#ifndef STRING_H_
#define STRING_H_

int strlen(const char *string);
int strcmp(const char *str1, const char *str2);
void strcpy(char *dest, const char *src);
char *strcat(char *dest, const char *src);

#endif // STRING_H_