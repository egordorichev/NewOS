#ifndef STDINT_H_
#define STDINT_H_

typedef char sint8_t;
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t; 
typedef unsigned size_t;

#endif // STDINT_H_