#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdint.h>

int memcmp(const void *aptr, const void *bptr, int size);
void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len);
void *memmove(void *dstptr, const void* srcptr, int size);
void *memset(void *bufptr, int value, int size);
void *malloc(int size);
void free(void *pointer);

#endif // MEMORY_H_