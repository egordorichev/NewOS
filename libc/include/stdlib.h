#ifndef STDLIB_H_
#define STDLIB_H_

#define RAND_MAX 32676

char *itoa(int number, int base);
int rand();
void srand(unsigned int seed);

#endif // STDLIB_H_