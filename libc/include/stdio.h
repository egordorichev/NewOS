#ifndef STDIO_H_
#define STDIO_H_

#include <stdarg.h>
#include <stdint.h>

void printf(const char *format, ...);
int sprintf(char *string, const char *format, ...);
char *readline();
char *readpass();
char getchar();
char getcharp();
uint8_t getscancode();

#endif // STDIO_H_