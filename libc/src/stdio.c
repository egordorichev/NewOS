#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include <memory.h>

#include <newOS/terminal.h>
#include <newOS/keyboard.h>
#include <newOS/terminal.h>

char *_convert(int n, int base){ 
	char *buf;
	unsigned long int tmp;
	int i, j;

	tmp = n;
	i = 0;

	do{
		tmp = n % base;
		buf[i++] = (tmp < 10) ? (tmp + '0') : (tmp + 'a' - 10);
	} while(n /= base);
	
	buf[i--] = 0;

	for(j = 0; j < i; j++, i--){
		tmp = buf[j];
		buf[j] = buf[i];
		buf[i] = tmp;
	}
	
	return buf;
}

void printf(const char *format, ...){
	va_list parameters;
	va_start(parameters, format);
 
	int number;
	double dnumber;
	terminal_t *terminal = get_currentterminal();

	for(; *format != '\0'; format++){
		if(*format != '%'){ 
			/*if(*format == '\x1B'){
				*format++;
				*format++;
	
				int color = *format - '0';
	
				// TODO: multisymbol numbers
	
				terminal_setcolor(color, VGA_BLACK);
			} else {*/
			terminal_putchar(terminal, *format);
			//}
			
			continue;
		}

		format++;

		switch(*format){
			case 'c': 
				terminal_putchar(terminal, va_arg(parameters, char));				
			break;
			case 's':
				terminal_write(terminal, va_arg(parameters, char *));
			break;
			case 'i':
				number = va_arg(parameters, int);

				if(number < 0){
					terminal_putchar(terminal, '-');
					number = -number;
				}

				terminal_write(terminal, _convert(number, 10));
			break;
			case 'd':
				dnumber = va_arg(parameters, double);

				if(dnumber < 0){
					terminal_putchar(terminal, '-');
					dnumber = -dnumber;
				}

				terminal_write(terminal, _convert(dnumber, 10));
			break;
			case 'x':
				terminal_write(terminal, "0x");
				terminal_write(terminal, _convert(va_arg(parameters, unsigned int), 16));
			break;
			case 'z':
				terminal_setcolor(terminal, va_arg(parameters, unsigned int), 0);
			break;
			default:
				// Unknown format
			break;
		}
	}
 
	va_end(parameters);
}

int sprintf(char *string, const char *format, ...){
	va_list parameters;
	va_start(parameters, format);
	
	char *str;
	int number;
	
	for(; *format != '\0'; *format++){		
		if(*format != '%'){
			*string++ = *format;
		} else {
			*format++;
	
			switch(*format){
				case 'c': 
					*string++ = va_arg(parameters, char);
				break;
				case 's':
					str = va_arg(parameters, char *);
					
					while(*str++){
						*string++ = *str;	
					}
				break;
				case 'i':

				break;
				case 'd':
					
				break;
				case 'x':
					
				break;
				case 'z':
					
				break;
				default:
					printf("Unknown format! \n"); // Unknown format
				break;
			}		
		}
	}
	
	va_end(parameters);
	
	return 0;
}

char *readline(){
	int buff_size = 256;
	char *line = malloc(buff_size);
	int line_length = 0;
	char c;

	while((c = getchar()) != '\n' && line_length < buff_size - 1){
		if(c == '\b'){
			if(line_length > 0){
				line_length--;
				terminal_putchar(get_currentterminal(), '\b');
			}
		} else {
			line[line_length] = c;
			line_length++;
		}
	}

	line[line_length] = '\0';

	return line;
}

char *readpass(){
	int buff_size = 256;
	char *line = malloc(buff_size);
	int line_length = 0;
	char c;

	while((c = getcharp()) != '\n' && line_length < buff_size - 1){
		if(c == '\b'){
			if(line_length > 0){
				line_length--;
				terminal_putchar(get_currentterminal(), '\b');
			}
		} else {
			line[line_length] = c;
			line_length++;
		}
	}

	line[line_length] = '\0';

	return line;	
}

char getchar(){
	char c = keyboard_getchar();

	while(c == 0){
		c = keyboard_getchar();
	}

	if(c >= ' '){
		terminal_putchar(get_currentterminal(), c);
	}

	return c;
}

char getcharp(){
	char c = keyboard_getchar();

	while(c == 0){
		c = keyboard_getchar();
	}

	if(c >= ' '){
		terminal_putchar(get_currentterminal(), '*');
	}

	return c;
}

uint8_t getscancode(){
	uint8_t c = keyboard_getscancode();

	while(c == 0){
		c = keyboard_getscancode();
	}

	return c;
}