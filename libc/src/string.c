#include <string.h>

int strlen(const char *string){
	int length = 0;
	
	while(string[length]){
		length++;
	}
	
	return length;
}

int strcmp(const char *str1, const char *str2){
	int res = 0;
	
	while(!(res = *(unsigned char *)str1 - *(unsigned char *)str2) && *str2){
		++str1;
		++str2;
	}
	
	if(res < 0){
		res = -1;
	}

	if(res > 0){
		res = 1;
	} 

	return res;
}

void strcpy(char *dest, const char *src){
	while(*dest++ = *src++);
}

char *strcat(char *dest, const char *src){
	for(int i = 0; dest[i] != '\0'; i++){
		int j;
		
		for(j = 0; src[j] != '\0'; j++){
			dest[i + j] = src[j];
		}
	
		dest[i + j] = '\0';
	}
	
	return dest;
}