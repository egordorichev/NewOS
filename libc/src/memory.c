#include <newOS/mm.h>

#include <memory.h>

int memcmp(const void *aptr, const void *bptr, int size){
	const unsigned char* a = (const unsigned char *)aptr;
	const unsigned char* b = (const unsigned char *)bptr;

	for(int i = 0; i < size; i++){
		if(a[i] < b[i]){
			return -1;
		} else if(b[i] < a[i]){
			return 1;
		}
	}

	return 0;
}

void *memset(void *bufptr, int value, int size){
	unsigned char *buf = (unsigned char *)bufptr;

	for(int i = 0; i < size; i++){
		buf[i] = (unsigned char)value;
	}

	return bufptr;
}

void *memmove(void *dstptr, const void *srcptr, int size){
	unsigned char *dst = (unsigned char *)dstptr;
	const unsigned char *src = (const unsigned char *)srcptr;

	if(dst < src)
		for(int i = 0; i < size; i++)
			dst[i] = src[i];
	else
		for(int i = size; i != 0; i--)
			dst[i-1] = src[i-1];
	return dstptr;
}

void memcpy(uint8_t *dest, const uint8_t *src, uint32_t len){
	const uint8_t *sp = (const uint8_t *)src;
	uint8_t *dp = (uint8_t *)dest;
	
	for(; len != 0; len--){
		*dp++ = *sp++;
	}
}

void *malloc(int size){
	return mm_malloc(size);	
}

void free(void *pointer){
	mm_free(pointer);
}