#include <stdlib.h>

static unsigned long int _next = 1;

char *itoa(int number, int base){
	char *buf;
	unsigned long int tmp;
	int i, j;

	tmp = number;
	i = 0;

	do{
		tmp = number % base;
		buf[i++] = (tmp < 10) ? (tmp + '0') : (tmp + 'a' - 10);
	} while(number /= base);
	
	buf[i--] = 0;

	for(j = 0; j < i; j++, i--){
		tmp = buf[j];
		buf[j] = buf[i];
		buf[i] = tmp;
	}
	
	return buf;
}
 
int rand(){
    _next = _next * 1103515245 + 12345;
    return (unsigned int)(_next / 65536) % RAND_MAX;
}
 
void srand(unsigned int seed){
	_next = seed;
}