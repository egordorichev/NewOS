# NewOS
_<a href='https://raw.githubusercontent.com/egordorichev/NewOS/master/screenshot.png'>Screenshot</a>_

My own OS written in C and Assembler!  

#### What already works

* It boots
* Kernel
* GDT
* IDT
* ISR
* IQR
* Keyboard input
* VGA output

#### Build and run

Run following commands:

```
make
make run
```

##### ISO

To create and run iso run following commands:

```
make createiso
make runiso
```
